\beamer@endinputifotherversion {3.20pt}
\beamer@sectionintoc {2}{Introduction}{3}{0}{1}
\beamer@subsectionintoc {2}{1}{Concept Drift}{3}{0}{1}
\beamer@subsectionintoc {2}{2}{Learning System}{4}{0}{1}
\beamer@sectionintoc {3}{Problem Definition}{5}{0}{2}
\beamer@subsectionintoc {3}{1}{Intuition}{5}{0}{2}
\beamer@subsectionintoc {3}{2}{Contributions}{6}{0}{2}
\beamer@subsectionintoc {3}{3}{Formal Problem Statement}{10}{0}{2}
\beamer@sectionintoc {4}{Related Work}{11}{0}{3}
\beamer@subsectionintoc {4}{1}{Data Mining Stand point}{11}{0}{3}
\beamer@subsectionintoc {4}{2}{Mathematical Stand point}{12}{0}{3}
\beamer@sectionintoc {5}{Subproblems}{14}{0}{4}
\beamer@sectionintoc {6}{Algorithm}{15}{0}{5}
\beamer@subsectionintoc {6}{1}{Framework}{15}{0}{5}
